class CreateCourseLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :course_logs do |t|
      t.integer :course_log_id
      t.string :course_log_type
    end
  end
end
