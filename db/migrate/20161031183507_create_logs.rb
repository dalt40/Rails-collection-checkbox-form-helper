class CreateLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :logs do |t|
      t.integer :log_id
      t.string :log_type
      t.timestamps
    end
  end
end
