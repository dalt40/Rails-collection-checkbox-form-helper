class CreateBadgeTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :badges_tasks do |t|
      t.integer :task_id
      t.integer :badge_id

      t.timestamps
    end
  end
end
