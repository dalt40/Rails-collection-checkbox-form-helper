class BadgeTask < ApplicationRecord
  has_one :badge, foreign_key: :id
  has_one :task, foreign_key: :id
end
