class Badge < ApplicationRecord
  has_many :badge_tasks, foreign_key: :task_id
  # has_many :tasks, through: :badge_tasks
  has_and_belongs_to_many :tasks
end
