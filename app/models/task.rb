class Task < ApplicationRecord
  has_many :badge_tasks, foreign_key: :badge_id
  # has_many :badges, through: :badge_tasks

  has_and_belongs_to_many :badges
end
