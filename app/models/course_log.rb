class CourseLog < ApplicationRecord
  actable as: :course_log
  acts_as :log, as: :log
end
