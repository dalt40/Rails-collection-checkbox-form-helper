class BadgeTasksController < ApplicationController
  before_action :set_badge_task, only: [:show, :edit, :update, :destroy]

  # GET /badge_tasks
  # GET /badge_tasks.json
  def index
    @badge_tasks = BadgeTask.all
  end

  # GET /badge_tasks/1
  # GET /badge_tasks/1.json
  def show
  end

  # GET /badge_tasks/new
  def new
    @badge_task = BadgeTask.new
  end

  # GET /badge_tasks/1/edit
  def edit
  end

  # POST /badge_tasks
  # POST /badge_tasks.json
  def create
    @badge_task = BadgeTask.new(badge_task_params)

    respond_to do |format|
      if @badge_task.save
        format.html { redirect_to @badge_task, notice: 'Badge task was successfully created.' }
        format.json { render :show, status: :created, location: @badge_task }
      else
        format.html { render :new }
        format.json { render json: @badge_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /badge_tasks/1
  # PATCH/PUT /badge_tasks/1.json
  def update
    respond_to do |format|
      if @badge_task.update(badge_task_params)
        format.html { redirect_to @badge_task, notice: 'Badge task was successfully updated.' }
        format.json { render :show, status: :ok, location: @badge_task }
      else
        format.html { render :edit }
        format.json { render json: @badge_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /badge_tasks/1
  # DELETE /badge_tasks/1.json
  def destroy
    @badge_task.destroy
    respond_to do |format|
      format.html { redirect_to badge_tasks_url, notice: 'Badge task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_badge_task
      @badge_task = BadgeTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def badge_task_params
      params.require(:badge_task).permit(:name, :task_id, :badge_id)
    end
end
