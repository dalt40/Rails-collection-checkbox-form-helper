json.array!(@badge_tasks) do |badge_task|
  json.extract! badge_task, :id, :name, :task_id, :badge_id
  json.url badge_task_url(badge_task, format: :json)
end
