require 'test_helper'

class BadgeTasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @badge_task = badge_tasks(:one)
  end

  test "should get index" do
    get badge_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_badge_task_url
    assert_response :success
  end

  test "should create badge_task" do
    assert_difference('BadgeTask.count') do
      post badge_tasks_url, params: { badge_task: { badge_id: @badge_task.badge_id, name: @badge_task.name, task_id: @badge_task.task_id } }
    end

    assert_redirected_to badge_task_url(BadgeTask.last)
  end

  test "should show badge_task" do
    get badge_task_url(@badge_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_badge_task_url(@badge_task)
    assert_response :success
  end

  test "should update badge_task" do
    patch badge_task_url(@badge_task), params: { badge_task: { badge_id: @badge_task.badge_id, name: @badge_task.name, task_id: @badge_task.task_id } }
    assert_redirected_to badge_task_url(@badge_task)
  end

  test "should destroy badge_task" do
    assert_difference('BadgeTask.count', -1) do
      delete badge_task_url(@badge_task)
    end

    assert_redirected_to badge_tasks_url
  end
end
